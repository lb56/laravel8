<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use App\Http\Controllers\ShowProfileController;
use App\Http\Controllers\PhotoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 *  定义你的路由模型绑定, pattern 过滤器等
 *
 * @return void
 */
Route::get('phpinfo',function (){
    dump(phpinfo());
});
//前台页面
Route::get('/',[\App\Http\Controllers\home\IndexController::class,'index']);
//疑难杂症
Route::get('/hard',[\App\Http\Controllers\home\IndexController::class,'hard']);
//留言
Route::get('/message',[\App\Http\Controllers\home\IndexController::class,'message']);
//about
Route::get('/about',[\App\Http\Controllers\home\IndexController::class,'about']);
//学无止境
Route::get('learn',[\App\Http\Controllers\home\IndexController::class,'learn']);
//发送邮件
Route::get('sendEmail',[\App\Http\Controllers\email\IndexController::class,'sendEmail']);
//qq登录
Route::get('qqBack',[\App\Http\Controllers\qq\QqController::class,'qqBack']);
Route::get('qq',[\App\Http\Controllers\qq\QqController::class,'qq']);

Route::get('ceshi',[\App\Http\Controllers\admin\Index1Controller::class,'ceshi']);
//layui 后台主页面
Route::get('/manage',[\App\Http\Controllers\admin\IndexController::class,'index']);
//后台默认展示内容
Route::get('/manage/welcome',[\App\Http\Controllers\admin\IndexController::class,'welcome']);
Route::get('manage/info',[\App\Http\Controllers\admin\IndexController::class,'info']);
//清理缓存
Route::get('/manage/clear',function(){
   Cache::flush();
   return ['code'=>1,'msg'=>'服务端清理缓存成功'];
});
//404页面
Route::fallback(function (){
    return view('admin.fail');
});
//系统设置
Route::get('/manage/system',[\App\Http\Controllers\admin\IndexController::class,'system']);

//权限路由
    //权限列表
   Route::any('/auth/index',[\App\Http\Controllers\admin\AuthController::class,'index']);
   Route::any('/auth/add',[\App\Http\Controllers\admin\AuthController::class,'add']);
   Route::any('/auth/edit',[\App\Http\Controllers\admin\AuthController::class,'edit']);
   Route::any('/auth/del',[\App\Http\Controllers\admin\AuthController::class,'del']);
   Route::get('/auth/info',[\App\Http\Controllers\admin\AuthController::class,'info']);

//用户路由
    Route::any('/user/index',[\App\Http\Controllers\admin\UserController::class,'index']);
//    Route::post('/user/info',[\App\Http\Controllers\admin\UserController::class,'info']);
    Route::any('/user/add',[\App\Http\Controllers\admin\UserController::class,'add']);
    Route::any('/user/edit',[\App\Http\Controllers\admin\UserController::class,'edit']);
    Route::post('/user/del',[\App\Http\Controllers\admin\UserController::class,'del']);
    Route::post('/user/check',[\App\Http\Controllers\admin\UserController::class,'check']);
    //登录
    Route::any('/manage/login',[\App\Http\Controllers\admin\UserController::class,'login']);
    Route::get('/manage/logout',function (Request $request){
        $request->session()->flush();
        $request->session()->save();
        return redirect('/manage/login');
    });

//角色路由
    Route::any('/role/index',[\App\Http\Controllers\admin\RoleController::class,'index']);
    Route::any('/role/add',[\App\Http\Controllers\admin\RoleController::class,'add']);
    Route::any('/role/edit',[\App\Http\Controllers\admin\RoleController::class,'edit']);
    Route::post('/role/del',[\App\Http\Controllers\admin\RoleController::class,'del']);
    Route::any('role/fp',[\App\Http\Controllers\admin\RoleController::class,'fp']);

Route::get('main',[\App\Http\Controllers\admin\IndexController::class,'main']);

//支付宝账号
Route::get('index',[\App\Http\Controllers\zfb\IndexController::class,'index']);
Route::get('zfb',[\App\Http\Controllers\zfb\IndexController::class,'zfb']);
Route::get('zfb/back',[\App\Http\Controllers\zfb\IndexController::class,'back']);


Route::get('/admin',function(){
    return view('admin.iframe');
});
Route::get('/admin/info',function(){
    return '{
        "homeInfo": {
            "title": "首页",
    "href": "/manage/welcome"
  },
  "logoInfo": {
            "title": "LAYUI MINI",
    "image": "/web/images/logo.png",
    "href": ""
  },
  "menuInfo": [
    {
        "title": "常规管理",
      "icon": "fa fa-address-book",
      "href": "",
      "target": "_self",
      "child": [
        {
            "title": "主页模板",
          "href": "",
          "icon": "fa fa-home",

          "child": [
            {
                "title": "主页一",
              "href": "page/welcome-1.html",
              "icon": "fa fa-tachometer"

            },
            {
                "title": "主页二",
              "href": "page/welcome-2.html",
              "icon": "fa fa-tachometer"

            },
            {
                "title": "主页三",
              "href": "page/welcome-3.html",
              "icon": "fa fa-tachometer"

            }
          ]
        },
        {
            "title": "菜单管理",
          "href": "page/menu.html",
          "icon": "fa fa-window-maximize",
          "target": "_self"
        },
        {
            "title": "系统设置",
          "href": "page/setting.html",
          "icon": "fa fa-gears",
          "target": "_self"
        },
        {
            "title": "表格示例",
          "href": "page/table.html",
          "icon": "fa fa-file-text",
          "target": "_self"
        },
        {
            "title": "表单示例",
          "href": "",
          "icon": "fa fa-calendar",
          "target": "_self",
          "child": [
            {
                "title": "普通表单",
              "href": "page/form.html",
              "icon": "fa fa-list-alt",
              "target": "_self"
            },
            {
                "title": "分步表单",
              "href": "page/form-step.html",
              "icon": "fa fa-navicon",
              "target": "_self"
            }
          ]
        },
        {
            "title": "登录模板",
          "href": "",
          "icon": "fa fa-flag-o",
          "target": "_self",
          "child": [
            {
                "title": "登录-1",
              "href": "page/login-1.html",
              "icon": "fa fa-stumbleupon-circle",
              "target": "_blank"
            },
            {
                "title": "登录-2",
              "href": "page/login-2.html",
              "icon": "fa fa-viacoin",
              "target": "_blank"
            },
            {
                "title": "登录-3",
              "href": "page/login-3.html",
              "icon": "fa fa-tags",
              "target": "_blank"
            }
          ]
        },
        {
            "title": "异常页面",
          "href": "",
          "icon": "fa fa-home",
          "target": "_self",
          "child": [
            {
                "title": "404页面",
              "href": "page/404.html",
              "icon": "fa fa-hourglass-end",
              "target": "_self"
            }
          ]
        },
        {
            "title": "其它界面",
          "href": "",
          "icon": "fa fa-snowflake-o",
          "target": "",
          "child": [
            {
                "title": "按钮示例",
              "href": "page/button.html",
              "icon": "fa fa-snowflake-o",
              "target": "_self"
            },
            {
                "title": "弹出层",
              "href": "page/layer.html",
              "icon": "fa fa-shield",
              "target": "_self"
            }
          ]
        }
      ]
    },
    {
        "title": "组件管理",
      "icon": "fa fa-lemon-o",
      "href": "",
      "target": "_self",
      "child": [
        {
            "title": "图标列表",
          "href": "page/icon.html",
          "icon": "fa fa-dot-circle-o",
          "target": "_self"
        },
        {
            "title": "图标选择",
          "href": "page/icon-picker.html",
          "icon": "fa fa-adn",
          "target": "_self"
        },
        {
            "title": "颜色选择",
          "href": "page/color-select.html",
          "icon": "fa fa-dashboard",
          "target": "_self"
        },
        {
            "title": "下拉选择",
          "href": "page/table-select.html",
          "icon": "fa fa-angle-double-down",
          "target": "_self"
        },
        {
            "title": "文件上传",
          "href": "page/upload.html",
          "icon": "fa fa-arrow-up",
          "target": "_self"
        },
        {
            "title": "富文本编辑器",
          "href": "page/editor.html",
          "icon": "fa fa-edit",
          "target": "_self"
        },
        {
            "title": "省市县区选择器",
          "href": "page/area.html",
          "icon": "fa fa-rocket",
          "target": "_self"
        }
      ]
    },
    {
        "title": "其它管理",
      "icon": "fa fa-slideshare",
      "href": "",
      "target": "_self",
      "child": [
        {
            "title": "多级菜单",
          "href": "",
          "icon": "fa fa-meetup",
          "target": "",
          "child": [
            {
                "title": "按钮1",
              "href": "page/button.html?v=1",
              "icon": "fa fa-calendar",
              "target": "_self",
              "child": [
                {
                    "title": "按钮2",
                  "href": "page/button.html?v=2",
                  "icon": "fa fa-snowflake-o",
                  "target": "_self",
                  "child": [
                    {
                        "title": "按钮3",
                      "href": "page/button.html?v=3",
                      "icon": "fa fa-snowflake-o",
                      "target": "_self"
                    },
                    {
                        "title": "表单4",
                      "href": "page/form.html?v=1",
                      "icon": "fa fa-calendar",
                      "target": "_self"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
            "title": "失效菜单",
          "href": "page/error.html",
          "icon": "fa fa-superpowers",
          "target": "_self"
        }
      ]
    }
  ]
}';
});
