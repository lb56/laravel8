<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>menu</title>
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/web/css/public.css" media="all">
    <link rel="stylesheet" href="/web/css/page.css?v=342343" media="all">
    <style>
        .layui-btn:not(.layui-btn-lg ):not(.layui-btn-sm):not(.layui-btn-xs) {
            height: 34px;
            line-height: 34px;
            padding: 0 8px;
        }
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div>
            <div class="layui-btn-group">
                <button class="layui-btn" id="btn-expand">全部展开</button>
                <button class="layui-btn layui-btn-normal" id="btn-fold">全部折叠</button>
            </div>
            <table id="munu-table" class="layui-table" lay-filter="munu-table"></table>
        </div>
    </div>
</div>
<!-- 操作列 -->
<script type="text/html" id="auth-state">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="add">添加</a>
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/web/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['table', 'treetable'], function () {
        var $ = layui.jquery;
        var table = layui.table;
        var treetable = layui.treetable;

        // 渲染表格
        layer.load(2);
        var re = treetable.render({
            treeColIndex: 1,
            treeSpid: 0,
            treeIdName: 'id',
            treePidName: 'pid',
            elem: '#munu-table',
            url: '{{ asset('auth/info') }}',
            page: false,
            cols: [[
                {type: 'numbers'},
                {field: 'auth_name', minWidth: 200, title: '权限名称'},
                {field: 'auth_url', minWidth: 200,title: '菜单url'},
                {field: 'order', width: 80, align: 'center', title: '排序号'},
                {
                    field: 'is_show', width: 80, align: 'center', templet: function (d) {
                        if (d.is_show == '0') {
                            return '<span class="layui-badge layui-bg-gray">按钮</span>';
                        }
                        if (d.is_show == -1) {
                            return '<span class="layui-badge layui-bg-blue">目录</span>';
                        } else {
                            return '<span class="layui-badge-rim">菜单</span>';
                        }
                    }, title: '类型'
                },
                {field: 'username',title:'最后修改人'},
                {templet: '#auth-state', width: 200, align: 'center', title: '操作'}
            ]],
            done: function () {
                layer.closeAll('loading');
            }
        });
        $('#btn-expand').click(function () {
            treetable.expandAll('#munu-table');
        });

        $('#btn-fold').click(function () {
            treetable.foldAll('#munu-table');
        });

        //监听工具条
        table.on('tool(munu-table)', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'del') {
                layer.confirm('将删除该权限及下级所有权限!',{
                    btn:['确定','取消']
                    }, function(){
                        $.post("{{ asset('/auth/del') }}",{id:data.id,_token:"{{ csrf_token() }}"},function(res){
                            if(res.code == 0){
                                layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                                    window.location.href = res.url;
                                });
                            }else{
                                layer.msg(res.msg,{time:1800,icon:2})
                            }
                        },'json')
                    },function() {
                    }
                )
            } else if (layEvent === 'edit') {
                layer.open({
                    type: 2,
                    area:['500px','500px'],
                    title:'编辑权限',
                    shadeClose:true,
                    content: "{{ asset('/auth/edit') }}?id="+data.id //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                });
            }else{
                layer.open({
                    type: 2,
                    area:['500px','500px'],
                    title:'添加权限',
                    shadeClose:true,
                    content: "{{ asset('/auth/add') }}?id="+data.id //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                });
            }
        });
    });
</script>
</body>
</html>
