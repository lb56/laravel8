<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <title>Document</title>
</head>
<body>
<form class="layui-form" action="" lay-filter="formDemo" style="width:400px">
    <div class="layui-form-item" style="margin-top:20px">
        <label class="layui-form-label">上级分类 <span style="color:red">*</span></label>
        <div class="layui-input-block">
            <select name="pid" lay-verify="required">
                <option value="0">顶级分类</option>
                @foreach ($authList as $v)
                    <option  value="{{ $v['id'] }}">{{ $v['auth_name'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">权限名称 <span style="color: red">*</span></label>
        <div class="layui-input-block">
            <input type="text" name="auth_name" required  lay-verify="required" placeholder="请输入权限名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">权限url</label>
        <div class="layui-input-inline">
            <input type="text" name="auth_url" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">默认为null</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-inline">
            <input type="text" name="order" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">从大到小</div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-inline">
            <input type="text" name="icon" value="0" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux"><a target="_blank" href="http://www.fontawesome.com.cn/faicons/">图标库</a></div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">后台是否展示</label>
        <div class="layui-input-block">
            <input type="checkbox"  name="is_show" lay-skin="switch">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/web/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    //Demo
    layui.use('form', function(){
        var $ = layui.jquery;
        var form = layui.form;
        var authinfo = {!! $authinfo !!}
        if(authinfo.is_show == '0'){
        //有这个值时，赋值form一直是点击状态
            delete authinfo.is_show   
        }
        form.val("formDemo", authinfo);
        //监听提交
        form.on('submit(formDemo)', function(data){
            // data = JSON.stringify(data.field)
            data.field.id = authinfo.id
            data.field._token = "{{ csrf_token() }}"
            $.post('{{ asset('/auth/edit') }}',data.field,function(res){
                if(res.code == 0){
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        //当你在iframe页面关闭自身时
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        parent.window.location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg, {time: 1800, icon: 2})
                }
            },'json')
            return false;
        });
    });
</script>
</body>
</html>
