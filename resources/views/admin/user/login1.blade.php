<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>后台管理-登陆</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {background-image:url("/web/images/bg.jpg");height:100%;width:100%;}
        #container{height:100%;width:100%;}
        input:-webkit-autofill {-webkit-box-shadow:inset 0 0 0 1000px #fff;background-color:transparent;}
        .admin-login-background {width:300px;height:300px;position:absolute;left:50%;top:40%;margin-left:-150px;margin-top:-100px;}
        .admin-header {text-align:center;margin-bottom:20px;color:#ffffff;font-weight:bold;font-size:40px}
        .admin-input {border-top-style:none;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;height:50px;width:300px;padding-bottom:0px;}
        .admin-input::-webkit-input-placeholder {color:#a78369}
        .layui-icon-username {color:#a78369 !important;}
        .layui-icon-username:hover {color:#9dadce !important;}
        .layui-icon-password {color:#a78369 !important;}
        .layui-icon-password:hover {color:#9dadce !important;}
        .admin-input-username {border-top-style:solid;border-radius:10px 10px 0 0;}
        .admin-input-verify {border-radius:0 0 10px 10px;}
        .admin-button {margin-top:20px;font-weight:bold;font-size:18px;width:300px;height:50px;border-radius:5px;background-color:#a78369;border:1px solid #d8b29f}
        .admin-icon {margin-left:260px;margin-top:10px;font-size:30px;}
        i {position:absolute;}
        .admin-captcha {position:absolute;margin-left:205px;margin-top:-40px;}
    </style>
</head>
<body>
<div id="container">
    <div></div>
    <div class="admin-login-background">
        <div class="admin-header">
            <span>layuimini</span>
        </div>
        <form class="layui-form" action="" style="background-color: #fff">
            <div>
                <i class="layui-icon layui-icon-username admin-icon"></i>
                <input type="text" name="username" oninput="usernameChange(this.value)" placeholder="请输入用户名" autocomplete="off" class="layui-input admin-input admin-input-username"  lay-verify="required|username" maxlength="15" >
                <span style="color: red;background-color: #fff" class="username"></span>
            </div>
            <div>
                <i class="layui-icon layui-icon-password admin-icon"></i>
                <input type="password" name="password" oninput="passChange(this.value)" placeholder="请输入密码" autocomplete="off" class="layui-input admin-input" lay-verify="required|username" maxlength="15">
                <span class="password" style="color: red;background-color: #fff"></span>
            </div>
            <div>
            </div>
            <div>
                <input type="text" name="captcha" placeholder="请输入验证码" autocomplete="off" class="layui-input admin-input admin-input-verify" lay-verify="required">
                <span class="captcha" style="color: red;background-color: #fff"></span>
                <img class="admin-captcha" width="90" height="30" onclick="this.src=this.src+'?'+Math.random()" src="{{captcha_src()}}">
            </div>
            <button class="layui-btn admin-button" lay-submit="" lay-filter="login">登 陆</button>
        </form>
    </div>
</div>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="https://v-cn.vaptcha.com/v3.js"></script>
<script>
    var $ = layui.jquery;
    //vaptcha
    var check = false;
    function doLogin(){
        //登录
        $.ajax({
            type: "POST",
            url:"{{ asset('/manage/login') }}",
            data:{username:$('input[name="username"]').val(),password:$('input[name="password"]').val(),_token:"{{ csrf_token() }}",captcha:$('input[name="captcha"]').val()},
            dataType:'json',
            success: function(res) {
                if(res.code == 0){
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        window.location.href = res.url;
                    });
                    return false
                }
                layer.msg(res.msg,{time:1800,icon:2})
            },
            error : function (msg) {
                if (msg.status == 422) {
                    var json=JSON.parse(msg.responseText);
                    json = json.errors;
                    for ( var item in json) {
                        for ( var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i],{time:1800,icon:2})
                            return ; //遇到验证错误，就退出
                        }
                    }

                } else {
                    alert('服务器连接失败');
                    return ;
                }
            }
        });
    }
    {{--vaptcha({--}}
    {{--    vid: '60f1173e7e40f8c915d72380', // 验证单元id--}}
    {{--    type: 'invisible', // 显示类型 隐藏式--}}
    {{--    scene: 0, // 场景值 默认0--}}
    {{--    //可选参数--}}
    {{--    //lang: 'auto', // 语言 默认auto,可选值auto,zh-CN,en,zh-TW,jp--}}
    {{--    //https: true, // 使用https 默认 true--}}
    {{--    //area: 'auto' //验证节点区域,默认 cn,可选值 auto,sea,na,cn--}}
    {{--}).then(function (vaptchaObj) {--}}
    {{--    obj = vaptchaObj //将VAPTCHA验证实例保存到局部变量中--}}
    {{--    //获取token的方式一：--}}
    {{--    //vaptchaObj.renderTokenInput('.login-form')//以form的方式提交数据时，使用此函数向表单添加server,token值--}}
    {{--    //获取token的方式二：--}}
    {{--    vaptchaObj.listen('pass', function () {--}}

    {{--        serverToken=vaptchaObj.getServerToken()--}}
    {{--        // 验证成功进行后续操作--}}
    {{--        var data = {--}}
    {{--            server:serverToken.server,--}}
    {{--            token: serverToken.token,--}}
    {{--        }--}}
    {{--        doLogin();--}}
    {{--        check = true--}}
    {{--        --}}{{--$.post('/manage/login', {username:$('input[name="username"]'),password:$('input[name="password"]'),_token:"{{ csrf_token() }}"}, function (r) {--}}
    {{--        --}}{{--    --}}
    {{--        --}}{{--})--}}
    {{--    })--}}
    {{--    //关闭验证弹窗时触发--}}
    {{--    vaptchaObj.listen('close', function () {--}}
    {{--        //验证弹窗关闭触发--}}
    {{--    })--}}
    {{--})--}}


    var usernameChange = function(username){
        if(username == '' || username == undefined || username == null){
            $('.username').text('');return false
        }
        var value = username;
        if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
            $('.username').text('用户名不能有特殊字符');return false
        }
        if(/(^\_)|(\__)|(\_+$)/.test(value)){
            $('.username').text('用户名首尾不能出现下划线\'_\'');return false
        }
        if(/^\d+\d+\d$/.test(value)){
            $('.username').text('用户名不能全为数字');return false
        }
        //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
        if(value === '呵呵'){
            $('.username').text('用户名不能为敏感词');return false
        }
        $('.username').text('')
    }
    function passChange(pass){
        if(pass == '' || pass == null || pass == undefined){
            return false
        }
        if(!(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/).test(pass)){
            $('.password').text('密码必须包含字母和数字，最少为6位,最多15位')
        }else{
            $('.password').text('')
        }
    }
    layui.use(['form','jquery'], function () {
        var $ = layui.jquery,
            form = layui.form,
            layer = layui.layer;

        // 登录过期的时候，跳出ifram框架
        if (top.location != self.location) top.location = self.location;

        $('.bind-password').on('click', function () {
            if ($(this).hasClass('icon-5')) {
                $(this).removeClass('icon-5');
                $("input[name='password']").attr('type', 'password');
            } else {
                $(this).addClass('icon-5');
                $("input[name='password']").attr('type', 'text');
            }
        });

        $('.icon-nocheck').on('click', function () {
            if ($(this).hasClass('icon-check')) {
                $(this).removeClass('icon-check');
            } else {
                $(this).addClass('icon-check');
            }
        });

        //自定义规则
        form.verify({
            username: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                    return '用户名不能有特殊字符';
                }
                if(/(^\_)|(\__)|(\_+$)/.test(value)){
                    return '用户名首尾不能出现下划线\'_\'';
                }
                if(/^\d+\d+\d$/.test(value)){
                    return '用户名不能全为数字';
                }
                //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
                if(value === '呵呵'){
                    return '用户名不能为敏感词';
                }
                if($(".username").text() != ''){
                    return '用户名已存在'
                }
            }

            //我们既支持上述函数式的方式，也支持下述数组的形式
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ,password: [
                /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/
                ,'密码必须包含字母和数字，最少为6位,最多15位'
            ]
            ,surepass:function (value,item){
                var password = $("input[name='password']").val();
                if(password !== value){
                    return '密码不一致'
                }
            }
        });
        // 进行登录操作
        form.on('submit(login)', function (data) {
            // if(!check){
            //     obj.validate()
            // }else{
            doLogin();
            // }
            return false;
        });

    });
</script>
</body>
</html>
