<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>后台管理-登陆</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .main-body {top:50%;left:50%;position:absolute;-webkit-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);transform:translate(-50%,-50%);overflow:hidden;}
        .login-main .login-bottom .center .item input {display:inline-block;width:227px;height:22px;padding:0;position:absolute;border:0;outline:0;font-size:14px;letter-spacing:0;}
        .login-main .login-bottom .center .item .icon-1 {background:url(/web/images/icon-login.png) no-repeat 1px 0;}
        .login-main .login-bottom .center .item .icon-2 {background:url(/web/images/icon-login.png) no-repeat -54px 0;}
        .login-main .login-bottom .center .item .icon-3 {background:url(/web/images/icon-login.png) no-repeat -106px 0;}
        .login-main .login-bottom .center .item .icon-4 {background:url(/web/images/icon-login.png) no-repeat 0 -43px;position:absolute;right:-10px;cursor:pointer;}
        .login-main .login-bottom .center .item .icon-5 {background:url(/web/images/icon-login.png) no-repeat -55px -43px;}
        .login-main .login-bottom .center .item .icon-6 {background:url(/web/images/icon-login.png) no-repeat 0 -93px;position:absolute;right:-10px;margin-top:8px;cursor:pointer;}
        .login-main .login-bottom .tip .icon-nocheck {display:inline-block;width:10px;height:10px;border-radius:2px;border:solid 1px #9abcda;position:relative;top:2px;margin:1px 8px 1px 1px;cursor:pointer;}
        .login-main .login-bottom .tip .icon-check {margin:0 7px 0 0;width:14px;height:14px;border:none;background:url(/web/images/icon-login.png) no-repeat -111px -48px;}
        .login-main .login-bottom .center .item .icon {display:inline-block;width:33px;height:22px;}
        .login-main .login-bottom .center .item {width:288px;height:35px;border-bottom:1px solid #dae1e6;margin-bottom:35px;}
        .login-main {width:428px;position:relative;float:left;}
        .login-main .login-top {height:117px;background-color:#148be4;border-radius:12px 12px 0 0;font-family:SourceHanSansCN-Regular;font-size:30px;font-weight:400;font-stretch:normal;letter-spacing:0;color:#fff;line-height:117px;text-align:center;overflow:hidden;-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0);}
        .login-main .login-top .bg1 {display:inline-block;width:74px;height:74px;background:#fff;opacity:.1;border-radius:0 74px 0 0;position:absolute;left:0;top:43px;}
        .login-main .login-top .bg2 {display:inline-block;width:94px;height:94px;background:#fff;opacity:.1;border-radius:50%;position:absolute;right:-16px;top:-16px;}
        .login-main .login-bottom {width:428px;background:#fff;border-radius:0 0 12px 12px;padding-bottom:53px;}
        .login-main .login-bottom .center {width:288px;margin:0 auto;padding-top:40px;padding-bottom:15px;position:relative;}
        .login-main .login-bottom .tip {clear:both;height:16px;line-height:16px;width:288px;margin:0 auto;}
        body {background:url(/web/images/loginbg.png) 0% 0% / cover no-repeat;position:static;font-size:12px;}
        input::-webkit-input-placeholder {color:#a6aebf;}
        input::-moz-placeholder {/* Mozilla Firefox 19+ */            color:#a6aebf;}
        input:-moz-placeholder {/* Mozilla Firefox 4 to 18 */            color:#a6aebf;}
        input:-ms-input-placeholder {/* Internet Explorer 10-11 */            color:#a6aebf;}
        input:-webkit-autofill {/* 取消Chrome记住密码的背景颜色 */            -webkit-box-shadow:0 0 0 1000px white inset !important;}
        html {height:100%;}
        .login-main .login-bottom .tip {clear:both;height:16px;line-height:16px;width:288px;margin:0 auto;}
        .login-main .login-bottom .tip .login-tip {font-family:MicrosoftYaHei;font-size:12px;font-weight:400;font-stretch:normal;letter-spacing:0;color:#9abcda;cursor:pointer;}
        .login-main .login-bottom .tip .forget-password {font-stretch:normal;letter-spacing:0;color:#1391ff;text-decoration:none;position:absolute;right:62px;}
        .login-main .login-bottom .login-btn {width:288px;height:40px;background-color:#1E9FFF;border-radius:16px;margin:24px auto 0;text-align:center;line-height:40px;color:#fff;font-size:14px;letter-spacing:0;cursor:pointer;border:none;}
        .login-main .login-bottom .center .item .validateImg {position:absolute;right:1px;cursor:pointer;height:36px;border:1px solid #e6e6e6;}
        .footer {left:0;bottom:0;color:#fff;width:100%;position:absolute;text-align:center;line-height:30px;padding-bottom:10px;text-shadow:#000 0.1em 0.1em 0.1em;font-size:14px;}
        .padding-5 {padding:5px !important;}
        .footer a,.footer span {color:#fff;}
        @media screen and (max-width:428px) {.login-main {width:360px !important;}
            .login-main .login-top {width:360px !important;}
            .login-main .login-bottom {width:360px !important;}
        }
    </style>
</head>
<body>
<div class="main-body">
    <div class="login-main">
        <div class="login-top">
            <span>LayuiMini后台登录</span>
            <span class="bg1"></span>
            <span class="bg2"></span>
        </div>
        <form class="layui-form login-bottom">
            <div class="center">
                <div class="item" style="margin-bottom:0">
                    <span class="icon icon-2"></span>
                    <input type="text" name="username" oninput="usernameChange(this.value)" lay-verify="required|username"  placeholder="请输入登录账号"  maxlength="15"/>
                </div>
                <div style="width:100%;height:35px"><span class="username" style="color: red"></span></div>

                <div class="item" style="margin-bottom:0px">
                    <span class="icon icon-3"></span>
                    <input type="password"  name="password" oninput="passChange(this.value)" lay-verify="required|password"  placeholder="请输入密码" maxlength="15">
                    <span class="bind-password icon icon-4"></span>
                </div>
                <div style="width: 100%;height: 35px"><span class="password" style="color: red"></span></div>
                <div id="validatePanel" class="item" style="width: 137px;">
                    <input type="text" name="captcha" lay-verify="required" placeholder="请输入验证码" maxlength="4">
                    <img id="refreshCaptcha" class="validateImg" onclick="this.src=this.src+'?'+Math.random()"  src="{{captcha_src()}}" >
                </div>

            </div>
            <div class="tip">
                <span class="icon-nocheck"></span>
                <span class="login-tip">保持登录</span>
                <a href="javascript:" class="forget-password">忘记密码？</a>
            </div>
            <div class="layui-form-item" style="text-align:center; width:100%;height:100%;margin:0px;">
                <button class="login-btn" lay-submit="" lay-filter="login">立即登录</button>
            </div>
        </form>
    </div>
</div>
<div class="footer">
    ©版权所有 2014-2018 叁贰柒工作室<span class="padding-5">|</span><a target="_blank" href="http://www.miitbeian.gov.cn">粤ICP备16006642号-2</a>
</div>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="https://v-cn.vaptcha.com/v3.js"></script>
<script>
    var $ = layui.jquery;
    //vaptcha
    vaptcha({
        vid: '60f1173e7e40f8c915d72380', // 验证单元id
        type: 'invisible', // 显示类型 隐藏式
        scene: 0, // 场景值 默认0
        //可选参数
        //lang: 'auto', // 语言 默认auto,可选值auto,zh-CN,en,zh-TW,jp
        //https: true, // 使用https 默认 true
        //area: 'auto' //验证节点区域,默认 cn,可选值 auto,sea,na,cn
    }).then(function (vaptchaObj) {
        obj = vaptchaObj //将VAPTCHA验证实例保存到局部变量中
        //获取token的方式一：
        //vaptchaObj.renderTokenInput('.login-form')//以form的方式提交数据时，使用此函数向表单添加server,token值
        //获取token的方式二：
        vaptchaObj.listen('pass', function () {

            serverToken=vaptchaObj.getServerToken()
            // 验证成功进行后续操作
            var data = {
                server:serverToken.server,
                token: serverToken.token,
            }
            //登录
            $.ajax({
                type: "POST",
                url:"{{ asset('/manage/login') }}",
                data:{username:$('input[name="username"]').val(),password:$('input[name="password"]').val(),_token:"{{ csrf_token() }}",code:$('input[name="captcha"]').val()},
                dataType:'json',
                success: function(res) {
                    if(res.code == 0){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            window.location.href = res.url;
                        });
                    }
                    layer.msg(res.msg,{time:1800,icon:2})
                },
                error : function (msg) {
                    if (msg.status == 422) {
                        var json=JSON.parse(msg.responseText);
                        json = json.errors;
                        for ( var item in json) {
                            for ( var i = 0; i < json[item].length; i++) {
                                layer.msg(json[item][i],{time:1800,icon:2})
                                return ; //遇到验证错误，就退出
                            }
                        }

                    } else {
                        alert('服务器连接失败');
                        return ;
                    }
                }
            });
            {{--$.post('/manage/login', {username:$('input[name="username"]'),password:$('input[name="password"]'),_token:"{{ csrf_token() }}"}, function (r) {--}}
            {{--    --}}
            {{--})--}}
        })
        //关闭验证弹窗时触发
        vaptchaObj.listen('close', function () {
            //验证弹窗关闭触发
        })
    })


var usernameChange = function(username){
    if(username == '' || username == undefined || username == null){
        $('.username').text('');return false
    }
    var value = username;
    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        $('.username').text('用户名不能有特殊字符');return false
    }
    if(/(^\_)|(\__)|(\_+$)/.test(value)){
        $('.username').text('用户名首尾不能出现下划线\'_\'');return false
    }
    if(/^\d+\d+\d$/.test(value)){
        $('.username').text('用户名不能全为数字');return false
    }
    //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
    if(value === '呵呵'){
        $('.username').text('用户名不能为敏感词');return false
    }
}
function passChange(pass){
    if(pass == '' || pass == null || pass == undefined){
        return false
    }
    if(!(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/).test(pass)){
        $('.password').text('密码必须包含字母和数字，最少为6位,最多15位')
    }else{
        $('.password').text('')
    }
}
    layui.use(['form','jquery'], function () {
        var $ = layui.jquery,
            form = layui.form,
            layer = layui.layer;

        // 登录过期的时候，跳出ifram框架
        if (top.location != self.location) top.location = self.location;

        $('.bind-password').on('click', function () {
            if ($(this).hasClass('icon-5')) {
                $(this).removeClass('icon-5');
                $("input[name='password']").attr('type', 'password');
            } else {
                $(this).addClass('icon-5');
                $("input[name='password']").attr('type', 'text');
            }
        });

        $('.icon-nocheck').on('click', function () {
            if ($(this).hasClass('icon-check')) {
                $(this).removeClass('icon-check');
            } else {
                $(this).addClass('icon-check');
            }
        });

        //自定义规则
        form.verify({
            username: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                    return '用户名不能有特殊字符';
                }
                if(/(^\_)|(\__)|(\_+$)/.test(value)){
                    return '用户名首尾不能出现下划线\'_\'';
                }
                if(/^\d+\d+\d$/.test(value)){
                    return '用户名不能全为数字';
                }
                //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
                if(value === '呵呵'){
                    return '用户名不能为敏感词';
                }
                if($(".username").text() != ''){
                    return '用户名已存在'
                }
            }

            //我们既支持上述函数式的方式，也支持下述数组的形式
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ,password: [
                /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/
                ,'密码必须包含字母和数字，最少为6位,最多15位'
            ]
            ,surepass:function (value,item){
                var password = $("input[name='password']").val();
                if(password !== value){
                    return '密码不一致'
                }
            }
        });
        // 进行登录操作
        form.on('submit(login)', function (data) {
            data = data.field;
                obj.validate()
            return false;
        });
    });
</script>
</body>
</html>
