<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <title>Document</title>
</head>
<body>
<form class="layui-form" action="" lay-filter="formDemo" style="width:400px">
    <div class="layui-form-item" style="margin-top:20px">
        <label class="layui-form-label">职位<span style="color:red">*</span></label>
        <div class="layui-input-block">
            <select name="role_id" lay-verify="required">
                @foreach ($roleList as $v)
                    <option  value="{{ $v['id'] }}">{{ $v['role_name'] }}</option>
                @endforeach
                    <option value="666666">前台注册账号</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户名 <span style="color: red">*</span></label>
        <div class="layui-input-block">
            <input type="text" oninput="usernameChange(this.value)"  name="username" required  lay-verify="required|username" maxlength="15" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            <span style="color: red" class="username"></span>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码<span style="color: red">*</span></label>
        <div class="layui-input-block">
            <input type="text"  name="password" oninput="passChange(this.value)" maxlength="15" placeholder="为空则不修改" lay-verify="password" autocomplete="off" class="layui-input">
            <span style="color: red;" class="password"></span>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">是否可以登录后台</label>
        <div class="layui-input-block">
            <input type="checkbox"  name="is_admin" lay-skin="switch">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/web/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    var $ = layui.jquery;
    var userinfo = {!! $userinfo !!};
    if(userinfo.is_admin == '0'){
        //有这个值时，赋值form一直是点击状态
        delete userinfo.is_admin   
    }
    var usernameChange = function(username){
        if(username == '' || username == undefined || username == null){
            $('.username').text('');return false
        }
        var value = username;
        if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
            $('.username').text('用户名不能有特殊字符');return false
        }
        if(/(^\_)|(\__)|(\_+$)/.test(value)){
            $('.username').text('用户名首尾不能出现下划线\'_\'');return false
        }
        if(/^\d+\d+\d$/.test(value)){
            $('.username').text('用户名不能全为数字');return false
        }
        //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
        if(value === '呵呵'){
            $('.username').text('用户名不能为敏感词');return false
        }
        if(username === userinfo.username){
            $('.username').text('')
            return false
        }
        $.post("{{ asset('/user/check') }}",{username:username,_token:"{{ csrf_token() }}"},function(e){
            if(e.code != '2'){
                $('.username').text(e.msg);
            }else{
                $('.username').text('')
            }
        },'json')
    }
    function passChange(pass){
        if(pass == '' || pass == null || pass == undefined){
            $('.password').text('')
            return false
        }
        if(!(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/).test(pass)){
            $('.password').text('密码必须包含字母和数字，最少为6位,最多15位')
        }else{
            $('.password').text('')
        }
    }

    //Demo
    layui.use('form', function(){
        var $ = layui.jquery;
        var form = layui.form;
        form.val('formDemo',userinfo)
        //自定义规则
        form.verify({
            username: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                    return '用户名不能有特殊字符';
                }
                if(/(^\_)|(\__)|(\_+$)/.test(value)){
                    return '用户名首尾不能出现下划线\'_\'';
                }
                if(/^\d+\d+\d$/.test(value)){
                    return '用户名不能全为数字';
                }
                //如果不想自动弹出默认提示框，可以直接返回 true，这时你可以通过其他任意方式提示（v2.5.7 新增）
                if(value === '呵呵'){
                    return '用户名不能为敏感词';
                }
                if($(".username").text() != ''){
                    return '用户名已存在'
                }
            }

            //我们既支持上述函数式的方式，也支持下述数组的形式
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ,password: function(value, item){ //value：表单的值、item：表单的DOM对象
                console.log(value)
                if(value != '' && !new RegExp("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$").test(value)){
                        return '密码必须包含字母和数字，最少为6位,最多15位';
                }
            }
        });


        //监听提交
        form.on('submit(formDemo)', function(data){
            // data = JSON.stringify(data.field)
            data.field._token = "{{ csrf_token() }}"
            data.field.id = userinfo.id
            $.post('{{ asset('/user/edit') }}',data.field,function(res){
                if(res.code == 0){
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        //当你在iframe页面关闭自身时
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        parent.window.location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1800,icon:2})
                }
            },'json')
            return false;
        });
    });
</script>
</body>
</html>
