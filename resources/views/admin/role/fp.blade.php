<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>树组件</title>
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
</head>
<body>
<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px;padding-left:10px;text-align: left">是否自动（获取/）去除子类</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-filter="filter" name="on" checked lay-skin="switch">
        </div>
    </div>
    <div class="layui-form-item">
        <div id="test1"></div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/web/lib/layui-v2.6.3/layui.js?v=342" charset="utf-8"></script>
<script>
    layui.use('tree', function(){
        var tree = layui.tree;
        // 获取权限树状图
        var $ = layui.jquery;
        var inst1 = tree.render({
            elem: '#test1'  //绑定元素
            ,showCheckbox: true
            ,id:'demoId'
            ,checkChirld:false
            ,data:{!! $authTree !!}
        });
        tree.setChecked('demoId', {!! $authList !!});
        inst1.config.checkChirld = true;//父级选中，链接子类
        //Demo
        var form = layui.form;
        //触发开关
        form.on('switch(filter)', function(data){
            // console.log(data.elem); //得到checkbox原始DOM对象
            // console.log(data.elem.checked); //开关是否开启，true或者false
            // console.log(data.value); //开关value值，也可以通过data.elem.value得到
            // console.log(data.othis); //得到美化后的DOM对象
            if(data.elem.checked){
                inst1.config.checkChirld = true;
            }else{
                inst1.config.checkChirld = false;
            }
        });

        //获取所有选中权限id
        function getChecked_list(data) {
            var id = "";
            $.each(data, function (index, item) {
                if (id != "") {
                    id = id + "," + item.id;
                }
                else {
                    id = item.id;
                }
                var i = getChecked_list(item.children);
                if (i != "") {
                    id = id + "," + i;
                }
            });
            return id;
        }
        //监听提交
        form.on('submit(formDemo)', function(data){
            var checkData = tree.getChecked('demoId');
            var id_list = getChecked_list(checkData)
            $.post("{{ asset('/role/fp') }}",{_token:"{{ csrf_token() }}",id:{{ $id }},id_list:id_list},function(res){
                if(res.code == 0){
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        //当你在iframe页面关闭自身时
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        parent.window.location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1800,icon:2})
                }
            },'json')
            return false;
        });
    });
</script>
</body>
</html>