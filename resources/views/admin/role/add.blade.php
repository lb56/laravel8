<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <title>Document</title>
</head>
<body>
<form class="layui-form" action="" style="width:400px">
    <div class="layui-form-item" style="margin-top:20px">
        <label class="layui-form-label">职位 <span style="color: red">*</span></label>
        <div class="layui-input-block">
            <input type="text"  name="role_name" required  lay-verify="required" maxlength="15" placeholder="请输入职位" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/web/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    //Demo
    layui.use('form', function(){
        var $ = layui.jquery;
        var form = layui.form;

        //监听提交
        form.on('submit(formDemo)', function(data){
            // data = JSON.stringify(data.field)
            data.field._token = "{{ csrf_token() }}"
            $.post('{{ asset('/role/add') }}',data.field,function(res){
                if(res.code == 0){
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        //当你在iframe页面关闭自身时
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                        parent.window.location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1800,icon:2})
                }
            },'json')
            return false;
        });
    });
</script>
</body>
</html>
