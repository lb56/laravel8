<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>menu</title>
    <link rel="stylesheet" href="/web/lib/layui-v2.6.3/css/layui.css" media="all">
    <link rel="stylesheet" href="/web/css/public.css" media="all">
    <link rel="stylesheet" href="/web/css/page.css?v=342343" media="all">
    <style>
        .layui-btn:not(.layui-btn-lg ):not(.layui-btn-sm):not(.layui-btn-xs) {
            height: 34px;
            line-height: 34px;
            padding: 0 8px;
        }
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">
        <div>
            <div class="layui-btn-group">
                <button class="layui-btn" id="add">添加职位</button>
                {{--                <button class="layui-btn layui-btn-normal" id="btn-fold">全部折叠</button>--}}
            </div>
            <table id="munu-table" class="layui-table" lay-filter="munu-table"></table>
        </div>
    </div>
</div>
<script src="/web/lib/layui-v2.6.3/layui.js" charset="utf-8"></script>
<script src="/web/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['table'], function () {
        var $ = layui.jquery;
        var table = layui.table;
        // 渲染表格
        table.render({
            elem: '#munu-table' //指定原始表格元素选择器（推荐id选择器)
            ,url: "{{ asset('/role/index') }}"
            ,method: 'post'
            ,page: true
            ,limit: 20
            ,cols: [[
                {field: 'id', title: 'ID', minWidth: 80},
                {field: 'role_name', title: '职位', minWidth: 120},
                {field: 'username', title: '最后修改人', minWidth: 200},
                {field: 'update_time', title: '更新时间',minWidth: 200},
                {minWidth: 200, align: 'center', title: '操作', templet: function(d){
                        if(d.id !== 1){
                            return '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="fp">分配权限</a>\n' +
                            '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>\n' +
                                '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>'
                        }
                        return '';
                    }
                }
            ]] //设置表头
            //,…… //更多参数参考右侧目录：基本参数选项
            ,done: function () {
                layer.closeAll('loading');
            }
        });
        //添加管理员
        $('#add').click(function (){
            layer.open({
                type: 2,
                area:['500px','500px'],
                title:'添加职位',
                shadeClose:true,
                content: "{{ asset('/role/add') }}" //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
            });
        })
        //监听工具条
        table.on('tool(munu-table)', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;
            if (layEvent === 'del') {
                layer.confirm('将删除该职位吗!',{
                        btn:['确定','取消']
                    }, function(){
                        $.post("{{ asset('/role/del') }}",{id:data.id,_token:"{{ csrf_token() }}"},function(res){
                            if(res.code == 0){
                                layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                                    window.location.href = res.url;
                                });
                            }else{
                                layer.msg(res.msg,{time:1800,icon:2})
                            }
                        },'json')
                    },function() {
                    }
                )
            } else if (layEvent === 'edit') {
                layer.open({
                    type: 2,
                    area:['500px','500px'],
                    title:'编辑职位',
                    shadeClose:true,
                    content: "{{ asset('/role/edit') }}?id="+data.id //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                });
            }else if(layEvent === 'fp'){
                layer.open({
                    type: 2,
                    area:['500px','500px'],
                    title:'分配权限',
                    shadeClose:true,
                    content: "{{ asset('/role/fp') }}?id="+data.id //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                });
            }
        });
    });
</script>
</body>
</html>
