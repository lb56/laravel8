<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="{{ asset('/web/lib/layui-v2.6.3/css/layui.css') }}">
    <link rel="stylesheet" href="{{ asset('/web/home/home.css?v=1.2.12') }}">
    <link rel="stylesheet" href="/web/home/swiper/dist/css/swiper.min.css">
    <link rel="icon" href="/web/images/logo.jpg" type="image/x-icon">
{{--    fa图标--}}
    <link rel="stylesheet" href="/web/lib/font-awesome-4.7.0/css/font-awesome.min.css">
{{--    看板娘--}}
    <link rel="stylesheet"  href="/web/kbn/live2d/assets/waifu.min.css?v=1.4.2"/>
    <!-- 引用看板娘交互组件 -->
    <script src="/web/kbn/live2d/assets/waifu-tips.min.js?v=1.4.2"></script>
    <script src="/web/kbn/live2d/assets/live2d.min.js?v=1.0.5"></script>
{{--    <script src='/web/kbn/static/pio.js'></script>--}}
    <script src="{{ asset('/web/lib/layui-v2.6.3/layui.js') }}"></script>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('/web/home/home.js?v=2.3.2') }}"></script>
</head>
<body>
<div style="position:fixed;top:0px;width: 100%;z-index: 1;">
    <div class="layui-progress" lay-filter="demo" lay-showPercent="yes" id="projress">
        <div class="layui-progress-bar layui-bg-red"  lay-percent="0" ></div>
    </div>
</div>


<div class="head">
    <div class="head-box">
        {{--        小于778--}}
        <div class="menu-pho">
            <a class="menu-i" href="javascript:toShow()"><i class="fa fa-list-ul"></i></a>
            <div onclick="toHide()">
                <ul>
                    <li><a href="/" @if(\Illuminate\Support\Facades\Request::path()  == '/')class="a-select"@endif>首页</a></li>
                    <li><a href="/hard" @if(\Illuminate\Support\Facades\Request::path()  == 'hard')class="a-select"@endif>疑难杂症</a></li>
                    <li><a href="/learn" @if(\Illuminate\Support\Facades\Request::path()  == 'learn')class="a-select"@endif>学无止境</a></li>
                    <li><a href="/about" @if(\Illuminate\Support\Facades\Request::path()  == 'about')class="a-select"@endif>关于我</a></li>
                    <li><a href="/message" @if(\Illuminate\Support\Facades\Request::path()  == 'message')class="a-select"@endif>留言</a></li>
                </ul>
            </div>
        </div>
        <a href="/" class="logo">
            <img src="/web/images/logo.jpg" alt="">
        </a>
        <div class="menu">
            <ul>
                <li><a href="/" @if(\Illuminate\Support\Facades\Request::path()  == '/')class="a-select"@endif>首页</a></li>
                <li><a href="/hard" @if(\Illuminate\Support\Facades\Request::path()  == 'hard')class="a-select"@endif>疑难杂症</a></li>
                <li><a href="/learn" @if(\Illuminate\Support\Facades\Request::path()  == 'learn')class="a-select"@endif>学无止境</a></li>
                <li><a href="/about" @if(\Illuminate\Support\Facades\Request::path()  == 'about')class="a-select"@endif>关于我</a></li>
                <li><a href="/message" @if(\Illuminate\Support\Facades\Request::path()  == 'message')class="a-select"@endif>留言</a></li>
                <li><a href=""><i class="layui-icon">&#xe615;</i></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="body-box">
    <div class="body-div">
        @yield('content')
    </div>
</div>
<div id="snowMask"></div>
<!-- waifu-tips.js 依赖 JQuery 库 -->
<script src="/web/kbn/live2d/assets/jquery.min.js?v=3.3.1"></script>

<!-- 实现拖动效果，需引入 JQuery UI -->
{{--<script src="/web/kbn/live2d/assets/jquery-ui.min.js?v=1.12.1"></script>--}}
<div class="waifu">
    <!-- 提示框 -->
    <div class="waifu-tips"></div>

    <!-- 看板娘画布 -->
    <canvas id="live2d" class="live2d" width="200" height="200"></canvas>

    <!-- 工具栏 -->
    <div class="waifu-tool">
        <span class="fui-home"></span>
        <span class="fui-chat"></span>
        <span class="fui-eye"></span>
        <span class="fui-user"></span>
        <span class="fui-photo"></span>
        <span class="fui-info-circle"></span>
        <span class="fui-cross"></span>
    </div>

</div>
</body>
</html>

{{--梅花--}}
<script type="text/javascript" src="/web/home/meihua.js?v=12.2"></script>
{{--<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/Ukenn2112/UkennWeb@3.0/index/web.js"></script>--}}
<script>
    layui.use('element', function(){
        var element = layui.element;
        $(window).scroll(function() {
            var show = $(window).height();
            var total = $(document).height();
            var top = $(window).scrollTop();
            if(top > 0){
                $('#projress').attr('style','display:block')
            }else{
                $('#projress').attr('style','display:none')
            }
            element.progress('demo', toPercent(top/(total - show)));
            // $('#projress').attr('lay-percent',toPercent(show/total))
        })
    })
    function toPercent(point) {
        var str = Number(point * 100).toFixed(2);
        str += "%";
        return str;
    }
    $(function(){
        /* 可直接修改部分参数 */
        live2d_settings['modelId'] = 1;                  // 默认模型 ID
        live2d_settings['modelTexturesId'] = 87;         // 默认材质 ID
        live2d_settings['modelStorage'] = false;         // 不储存模型 ID
        live2d_settings['canCloseLive2d'] = true;       // 隐藏 关闭看板娘 按钮
        live2d_settings['canTurnToHomePage'] = false;    // 隐藏 返回首页 按钮
        live2d_settings['waifuSize'] = '200*200';        // 看板娘大小
        live2d_settings['waifuTipsSize'] = '200*150';    // 提示框大小
        live2d_settings['waifuFontSize'] = '16px';       // 提示框字体
        live2d_settings['waifuToolFont'] = '16px';       // 工具栏字体
        live2d_settings['waifuToolLine'] = '20px';       // 工具栏行高
        live2d_settings['waifuToolTop'] = '-60px';       // 工具栏顶部边距
        live2d_settings['waifuDraggable'] = 'axis-x';    // 拖拽样式
        /* 在 initModel 前添加 */
        initModel("/web/kbn/live2d/assets/waifu-tips.json?v=1.4.2")
    })
</script>
