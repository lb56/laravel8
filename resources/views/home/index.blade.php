@extends('home.common.head')


@section('content')
    <style>
        /*.swiper-container .swiper-wrapper{*/
        /*    height:250px;overflow: hidden*/
        /*}*/
        .swiper-container .swiper-wrapper .swiper-slide img{
            width: 100%;
            height: 250px;
        }
    </style>
        <div class="content-left">
            <div class="swiper-container" style="z-index: 0">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="/web/images/banner/26de7131077590503be8adb0c96f8144.jpg" alt=""></div>
                    <div class="swiper-slide"><img src="/web/images/banner/9750aaca2a1b75a3763d680eae27e763.jpeg" alt=""></div>
                    <div class="swiper-slide"><img src="/web/images/banner/d646badae7988291fd6302fdac7256a0.jpg" alt=""></div>

                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
            <script src="/web/home/swiper/dist/js/swiper.min.js"></script>
            <script>
                var mySwiper = new Swiper('.swiper-container', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    autoplay: 5000,//可选选项，自动滑动
                    loop:true
                })
            </script>
            <div class="article-list">
                <ul>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                    <li class="article-list-li">
                        <p>
                            <a href="">
                                <span class="sign">【热】</span> <span class="title">我是标题</span>
                            </a>
                        </p>
                        <div class="article-con">
                            <div class="left">
                                <img src="https://www.zhangqinblog.com//./templates/Images/news1.jpg" alt="">
                            </div>
                            <div class="right">
                                <p>生活如海，青春作舟，泛舟于海，方知海之宽阔；生活如山，青春为径，寻径登山，方知山之巍峨；生活如歌，青春作曲，按曲而歌，方知歌之灵动。青春是我们人生中一笔宝贵的财富，我们应该好好珍惜！</p>
                                <p>
                                    <span style="color:dodgerblue">梦无止境</span>
                                    <span> 2021.8.9</span>
                                    <span><i class="fa fa-eye" style="color:dodgerblue"></i> 50</span>
                                    <span><a href=""><i class="layui-icon" style="color: greenyellow">&#xe6c6;</i></a> 55</span>
                                    <span><a href="">阅读</a></span>
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-right">
            <div class="peo-info">
                <p class="title">个人博客</p>
                <p class="content">职业 : 迷茫的程序员,phper,js,学习中</p>
                <p class="content">现居 : 上海</p>
                <p class="content">Email ： 2966723407@qq.com</p>
                <ul>
                    <li><a class="layui-icon" href="/"><svg t="1627632608110" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1128" width="200" height="200"><path d="M931.083286 452.28906 591.528799 101.976556c-21.101597-21.747303-49.296753-33.726126-79.520101-33.726126-30.241768 0-58.4359 11.978822-79.537498 33.726126L92.915691 452.28906c-35.035958 36.150338-30.528294 67.396993-24.728194 80.453357 4.130064 9.267061 18.065448 34.731012 58.221006 34.731012l49.762357 0 0 269.464151c0 61.201897 44.321438 118.811989 107.319238 118.811989l57.125045 0 87.421049 0 0-63.321164L428.036191 670.553109c0-30.583552-4.669346-47.625694 26.847461-47.625694l57.125045 0L569.115324 622.927415c31.516808 0 26.847461 17.042141 26.847461 47.625694l0 221.875297 0 63.321164 87.421049 0 57.126069 0c62.997799 0 107.336634-57.611116 107.336634-118.811989L847.846536 567.473429l49.762357 0c40.137139 0 54.089919-25.464975 58.20361-34.731012C961.612603 519.686053 966.120266 488.439398 931.083286 452.28906z" p-id="1129" fill="#d81e06"></path></svg></a></li>
                    <li><a target="_blank" href="http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=m6mira2sqaivq6zb6uq1_PT2" style="text-decoration:none;"><svg t="1627633113016" class="icon" viewBox="0 0 1113 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4403" width="200" height="200"><path d="M1110.075362 5.194203l-0.742029 3.710145-1.484058 5.936232-196.637681 960.185507c-4.452174 21.518841-28.93913 31.907246-47.489855 20.776812l-313.136232-187.733334-0.742029-0.742029c-0.742029 0-0.742029-0.742029-1.484058-1.484058l-3.710145 5.194203-30.423188 41.553623c-0.742029 0.742029-1.484058 2.226087-2.226087 2.226087l-114.272464 158.052174c-8.904348 12.614493-28.93913 6.678261-29.681159-8.904348l-10.388406-287.907246v-0.742029L23.744928 601.043478c-26.713043-8.904348-28.93913-46.005797-3.710145-58.62029L1110.075362 5.194203z" fill="#6097FD" p-id="4404"></path><path d="M1110.075362 8.904348l-1.484058 5.936232-2.226087 9.646377-558.005797 782.09855-148.405797 203.315942c-11.130435 11.130435 57.136232-76.428986-1.484058 4.452174-8.904348 12.614493-28.93913 6.678261-29.681159-8.904348l-10.388406-287.907246L1110.075362 8.904348z" fill="#2767F4" p-id="4405"></path></svg></a></li>
                    <li><a href="tencent://message/?uin=2966723407&Site=Sambow&Menu=yes" class="layui-icon"><svg t="1627633197612" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7036" width="200" height="200"><path d="M68.399765 590.615655c-37.073602 90.765025-43.465533 176.418105-14.062849 191.757941 20.45478 11.505876 53.692423-14.061849 84.374094-60.084355 11.504876 51.135451 42.186547 95.87897 84.373094 132.952572-44.743519 16.619821-74.146204 44.743519-74.146204 75.42519 0 51.135451 79.259149 93.321998 176.418105 93.321997 88.208052 0 161.07627-33.237643 175.138119-77.982162h20.45478C535.009753 990.751357 607.87897 1023.989 696.087023 1023.989c98.436943 0 176.418105-40.908561 176.418104-93.321997 0-30.68167-29.402684-58.806368-74.146203-75.42519 42.186547-37.073602 72.868217-81.817121 84.374094-132.952572 30.68067 46.022506 62.640327 71.589231 84.373093 60.084355 30.68167-15.339835 24.289739-102.270901-14.061849-191.757941-29.403684-70.311245-69.033258-122.725682-99.714929-134.230558 0-3.835959 1.278986-8.949904 1.278987-14.062849 0-26.845712-7.669918-52.413437-20.454781-72.868217v-5.112945c0-12.783863-2.555973-24.289739-7.669917-34.516629C818.813704 145.736434 701.200968 0 510.722014 0 320.24206 0 202.630323 145.736434 194.959406 329.824457c-5.112945 10.22689-7.669918 21.732767-7.669918 34.516629v5.112945c-12.783863 20.45478-20.45478 46.022506-20.45478 72.869217v14.061849c-28.123698 11.504876-69.032258 62.640327-98.434943 134.230558z" fill="#4CAFE9" p-id="7037"></path></svg></a></li>
                    <li><a href="" class="layui-icon"><svg t="1627633145710" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5327" width="200" height="200"><path d="M1010.8 628c0-141.2-141.3-256.2-299.9-256.2-168 0-300.3 115.1-300.3 256.2 0 141.4 132.3 256.2 300.3 256.2 35.2 0 70.7-8.9 106-17.7l96.8 53-26.6-88.2c70.9-53.2 123.7-123.7 123.7-203.3zM618 588.8c-22.1 0-40-17.9-40-40s17.9-40 40-40 40 17.9 40 40c0 22-17.9 40-40 40z m194.3-0.3c-22.1 0-40-17.9-40-40s17.9-40 40-40 40 17.9 40 40-17.9 40-40 40z" fill="#00C800" p-id="5328"></path><path d="M366.3 106.9c-194.1 0-353.1 132.3-353.1 300.3 0 97 52.9 176.6 141.3 238.4l-35.3 106.2 123.4-61.9c44.2 8.7 79.6 17.7 123.7 17.7 11.1 0 22.1-0.5 33-1.4-6.9-23.6-10.9-48.3-10.9-74 0-154.3 132.5-279.5 300.2-279.5 11.5 0 22.8 0.8 34 2.1C692 212.6 539.9 106.9 366.3 106.9zM247.7 349.2c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48z m246.6 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48z" fill="#00C800" p-id="5329"></path></svg></a></li>
                </ul>
            </div>
        </div>

@endsection
