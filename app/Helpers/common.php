<?php
/**
 * 根据pid遍历$arr数组
 * @param $arr 要处理的数组
 * @param $pid 顶级pid
 */
    function getChildTree($arr,$pid=0){
        $result = [];
        foreach($arr as $k=>$v){
            if($v['pid'] == $pid){
                $v['child'] = getChildTree($arr,$v['id']);
                if($v['child'] == []){
                    unset($v['child']);
                }
                $result[] = $v;
            }
        }
        return $result;
    }
    function getAuthSelectList($arr,$pid=0,$leftHtml='-|',$leftnum=0){
        $result = [];
        foreach($arr as $k=>$v){
            if($v['pid'] == $pid){
                $result[] = [
                    'auth_name' => str_repeat($leftHtml,$leftnum).$v['auth_name'],
                    'id' => $v['id']
                ];
                $result = array_merge($result,getAuthSelectList($arr,$v['id'],$leftHtml,$leftnum+1));
            }

        }
        return $result;
    }

//树状图 权限 分配权限
function getAuthTree($arr,$pid=0){
        $result = [];
        foreach($arr as $k=>$v){
            if($v['pid'] == $pid){
                $v['title'] = $v['auth_name'];//权限名称
                $v['spread'] = true;//展开tree
                $v['children'] = getAuthTree($arr,$v['id']);
                if($v['children'] == []){
                    unset($v['children']);
                }
                $result[] = $v;
            }
        }
        return $result;
}