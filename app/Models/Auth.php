<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use DateTimeInterface;
class Auth extends Model
{
    use HasFactory;
    protected $table = 'lb_auth';
    protected $primarykey = 'id';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    /**
     * @return start 配置后台主页参数
     */
    public static function setAuthinfo(){
        Cache::flush();
        if(!Cache::has('authinfoAdmin')){
            return self::getAuthinfo();
        }
        return Cache::get('authinfoAdmin');
    }
    protected static function getAuthinfo(){
        $authinfo = self::where('status','=','1')->where('is_show','=','1')
            ->orderBy('order', 'desc')
            ->select('auth_name as title','auth_url as href','icon','id','pid')
            ->get()->toArray();
        //权限树状图
        $authinfo_tree = getChildTree($authinfo);
        $arr = self::system();
        $arr['menuInfo'] = $authinfo_tree;
        $arr = json_encode($arr);
        Cache::forever('authinfoAdmin',$arr);
        return $arr;
    }
    protected static function system(){
        $arr = [];
        $arr['homeInfo']['title'] = '首页';
        $arr['homeInfo']['href'] = '/manage/welcome';
        $arr['logoInfo']['title'] = 'layui mini';
        $arr['logoInfo']['image'] = 'web/images/logo.png';
        $arr['logoInfo']['href'] = '';
        return $arr;
    }
    /**
     * end
     */

    //start  获取layui tabletree 数据
    public static function setTableTree(){
        if(!Cache::has('tableinfo')){
            return self::getTableTree();
        }
        return Cache::get('tableinfo');
    }

    public static function getTableTree(){
        $result = self::where('status','=','1')->orderby('order','desc')->get()->toArray();
        Cache::forever('tableinfo',$result);
        return $result;
    }
    //end


    //start 权限选择框遍历数据   权限上级分类
    public static function setAuthList(){
        if(!Cache::has('authList')){
            return self::getAuthList();
        }
        return Cache::get('authList');
    }
    public static function getAuthList(){
        $result = self::where('status','=','1')->orderby('order','desc')->select('id','pid','auth_name')->get()->toArray();
        //把数组改为自己想要的样式
        $result = getAuthSelectList($result,0);
        return $result;
    }

    //分配权限 树状tree
    public static function getAuthTree(){
        $result = self::where('status','=','1')->orderby('order','desc')->select('id','pid','auth_name')->get()->toArray();
//        $result = getAuthTree($result,0);
        $result = json_encode(getAuthTree($result,0));
        return $result;
    }
}
