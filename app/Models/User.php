<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use DateTimeInterface;
class User extends Model
{
    use HasFactory;
    protected $table = 'lb_user';
    protected $primarykey = 'id';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    public static function getUserList($page,$limit){
        $userInfos = User::leftJoin('lb_role','lb_user.role_id','=','lb_role.id')->select('lb_user.*','lb_role.role_name')->where('lb_user.status','=','1')->orderBy('lb_user.is_admin','asc')->orderBy('lb_user.role_id','asc')->orderBy('id','asc')->offset($page)->limit($limit)->get()->toArray();
        return $userInfos;
    }



}
