<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
use Illuminate\Support\Facades\Cache;
class Role extends Model
{
    use HasFactory;
    protected $table = 'lb_role';
    protected $primarykey = 'id';
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
    public static function getRoleList(){
        if(Cache::has('roleList')){
            return Cache::get('roleList');
        }
        $roleList = Role::where('status','=','1')->get();
        Cache::forever('roleList',$roleList);
        return $roleList;
    }
    public static function getRoleInfos($page,$limit){
        $roleInfos = self::leftJoin('lb_user','lb_user.id','=','lb_role.last_update_id')->select('lb_role.*','lb_user.username')->where('lb_role.status','=','1')->offset($page)->limit($limit)->get()->toArray();
        return $roleInfos;
    }
}
