<?php

namespace App\Http\Controllers\zfb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alipay\EasySDK\Kernel\Factory;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;
use Alipay\EasySDK\Kernel\Config;
class IndexController extends Controller
{
    public function index(){
        //1. 设置参数（全局只需设置一次）
        Factory::setOptions($this->getOptions());
        try {
            //2. 发起API调用（以支付能力下的统一收单交易创建接口为例）
            $result = Factory::payment()->wap()->pay("iPhone6 16G", "20200326235526003", "0.01", "http://laravel.liubing.fun/zfb/back","http://laravel.liubing.fun/zfb/back");
            $responseChecker = new ResponseChecker();
            //3. 处理响应或异常
            if ($responseChecker->success($result)) {
                echo "调用成功" . PHP_EOL;
                return $result->body;
            } else {
                echo "调用失败，原因：" . $result->msg . "，" . $result->subMsg . PHP_EOL;
            }
        } catch (Exception $e) {
            echo "调用失败，" . $e->getMessage() . PHP_EOL;;
        }

    }

    public function zfb()
    {
        //1. 设置参数（全局只需设置一次）
        Factory::setOptions($this->getOptions());
        try {
            //2. 发起API调用（以支付能力下的统一收单交易创建接口为例）
            $result = Factory::payment()->page()->pay("iPhone6 16G", "20200326235526004", "0.01", "http://laravel.liubing.fun/zfb/back");
            $responseChecker = new ResponseChecker();
            //3. 处理响应或异常
            if ($responseChecker->success($result)) {
                echo "调用成功" . PHP_EOL;
//                dump($result);die;
                return $result->body;
            } else {
                echo "调用失败，原因：" . $result->msg . "，" . $result->subMsg . PHP_EOL;
            }
        } catch (Exception $e) {
            echo "调用失败，" . $e->getMessage() . PHP_EOL;;
        }
    }
    public function back(Request $request){
        dump($request->all());
    }
    /**
     * 支付宝配置信息
     */
    protected static function getOptions(){
        $options = new Config();
        $options->protocol = 'https';
        $options->gatewayHost = 'openapi.alipaydev.com';
//        $options->gatewayHost = 'openapi.alipay.com';
        $options->signType = 'RSA2';
        $options->appId = '2021000117695541';
        // 为避免私钥随源码泄露，推荐从文件中读取私钥字符串而不是写入源码中
        $options->merchantPrivateKey = 'MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQD0Ywk1b81DqC3phEFnoul0N5bHNubbwlbe+3NMttEoiVeMqoo5lfEah1pccVp1oiMhId/ZE9eY/NcbsPn8SQ6N40rnp1B45l+oW6pKSn0cYoY75u+H+a9hoZ82V2TyCMu7WfsxBi4IgQKEDOonp6cA1x3xpHvRm/D8g/KmJ9foYvujeKjcMoVT61NBu8f6NvRcz7jFgwtpHydBGknP4DBa79kyRsV6g3zhsYHeiVRc6ttPjsrjyIBLtNutYNU0NpkRqlPhiqYQ9Y15tWjch6uZwJ3E4BzpljjwWsm6vF9HAUpBrKOoM66k3P0AJJaCS24gkFtd3NyMAgflnn4k6r9fAgMBAAECggEBAJZJKOqRnFsU21cVG9uNlZ8AaFqN6p4N/roVS6xLaIhTHWHFpkcPbDtyLSFYG9bnJE9Fs5Cc4SKVqfq/dJuumaKrue6HN7eBhCXWaPxSnmmMNRw1QewOocmaSHx0dgFZY2ANT6mwMTLNkyx9fKlyMgMURrfI/NPD4AiJY/jF28JtmMVehn3MZfuLtVnsw3mbYy2jpQcmsJJyUokYsgHf6OqMVEC5fId6RRHMAXFhlUE5Q75SVj07+3hb/ePQDjiy7zWrDNh+I3E7gc22fSvxnT68oZ/2jcLN9/d7mqy0n7JFRaMxVlMm2zs/XnIwLlJIwu5969JFDIPxXHxUc4oeZaECgYEA/TPfwSrlo9qZSpz6xEADs61PJH+q2EvYCNUfhPNntql6u41kQsTkVjZeHT3AlcuM+D21Q6JjrsmO6ad/S/HO1xx6QK5t24p+h7rcNLE8QnrMDI9szgCTw4r4FZcLcX8V383upNEkIfzLzcuHRJOWkRcxdkfPrSiYBD2MYVt/OMkCgYEA9xY6gHuH7kRsL476epwciGTCR81I2TnK9KEJZac3Ml6bMpLfQBiH1eixPjO9DXfAg+50pEL3vJUk7XAjaBjRR0IFO8rHwBlk814eIWpGxB44I1CPT07fZzJEXYVgle0v7fY7eg3LjgPjcF4aImqNPo7SWPv6KYADBQFPnR6tcucCgYEAs8bVJA4VJpXbKUL0OctJ1ZVEECIlZ5XSOdM4oelAxkznO6tUeKPbi+rmP9TMnI799HO7892k8UFjGzs2AvIZblpl4u72SV64IB7oovEEXB9cKb7b/hgi0kAljQ7FrTwSh+lre9KS2R7ohaHw1RwFV9cj41txdPzRkpEwzx9r4SECgYEAtRnayT9Bs1b9QKJ4XpO7qhDIAV6qjWT5p/G8dIBqSYXkFN5nuZABR7zk5GWBpLRoK4sEfT0EXXmndkyh9vNxuNvgACIx5E/4mRQb23wM4EQR+3hZgdSalAXY6/8NevY9EhpPH7eqwa2B72NZnm4gloCSFlyhz/cev3VliBDrKnMCgYBk6h6yyr9IJjCOpetheUqDOZekGgm2j9/ms+CSHhqJZWTkwNJ/7y7LuohlASTzoSMfukQ5T3j2IgoVcUV591R9UkL1k+1JA36RQJPKgqUCxTb+EfVXJBZo/QsgiJtc1d1yOiRT/VFrb0KOMVianasNOQezXB2HqxHblK6ySJm9PQ==';
        $options->alipayCertPath = 'web/zfb/alipayCertPublicKey_RSA2.crt';
        $options->alipayRootCertPath = 'web/zfb/alipayRootCert.crt';
        $options->merchantCertPath = 'web/zfb/appCertPublicKey_2021000117695541.crt';
        //注：如果采用非证书模式，则无需赋值上面的三个证书路径，改为赋值如下的支付宝公钥字符串即可
//         $options->alipayPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9GMJNW/NQ6gt6YRBZ6LpdDeWxzbm28JW3vtzTLbRKIlXjKqKOZXxGodaXHFadaIjISHf2RPXmPzXG7D5/EkOjeNK56dQeOZfqFuqSkp9HGKGO+bvh/mvYaGfNldk8gjLu1n7MQYuCIEChAzqJ6enANcd8aR70Zvw/IPypifX6GL7o3io3DKFU+tTQbvH+jb0XM+4xYMLaR8nQRpJz+AwWu/ZMkbFeoN84bGB3olUXOrbT47K48iAS7TbrWDVNDaZEapT4YqmEPWNebVo3IermcCdxOAc6ZY48FrJurxfRwFKQayjqDOupNz9ACSWgktuIJBbXdzcjAIH5Z5+JOq/XwIDAQAB';
        //可设置异步通知接收服务地址（可选）
        $options->notifyUrl = "http://laravel.liubing.fun/zfb/back";
        //可设置AES密钥，调用AES加解密相关接口时需要（可选）
        $options->encryptKey = "";
        return $options;
    }
}
