<?php

namespace App\Http\Controllers\qq;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
class QqController extends Controller
{
    //
    public function qq(){
        return Socialite::with('qq')->redirect();
    }
    public function qqBack(){
        $user = Socialite::driver('qq')->user();
        dd($user);
    }
}
