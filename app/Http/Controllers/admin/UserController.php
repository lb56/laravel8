<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
class UserController extends BaseController
{

    //后台登录页面
    public function login(Request $request){
        if($request->isMethod('post')){
            $message = [
                'username.required'=>'用户名必填',
                'password.required'=>'密码必填',
                'captcha'=>'验证码错误',
            ];
            $this->validate($request,[
                'username'=>'bail|required',
                'password'=>'bail|required',
                'captcha'=>'bail|required|captcha'
            ],$message);
//            判断用户是否存在
            $userinfo = User::where('username','=',$request->username)->first();
            if(!$userinfo){
                return $this->error('账号或密码错误');
            }
            if($userinfo['status'] === '0'){
                return $this->error('该账号已禁用');
            }
            if(!Hash::check(md5('lb'.$request->password),$userinfo['password'])){
                return $this->error('密码错误');
            }
            $request->session()->flush();
            $request->session()->save();
            session(['user'=>$userinfo->toArray()]);
            return $this->success('登陆成功',asset('/manage'));
        }
        return view('admin.user.login1');
    }

    public function index(Request $request){
        if($request->isMethod('post')){
            $page = $request->input('page',1);
            $limit = $request->input('limit',20);
            $page = ($page-1)*$limit;;
            $result = User::getUserList($page,$limit);
            return $this->showList($result,count(User::where('status','=',1)->get()));
        }
        return view('admin.user.index');
    }


    public function info(Request $request){
        $page = $request->input('page',1);
        $limit = $request->input('limit',20);
        $page = ($page-1)*$limit;;
        $result = User::getUserList($page,$limit);
        return $this->showList($result,count(User::where('status','=',1)->get()));
    }


    public function check(Request $request){
        if($request->filled('username')){
           $username = $request->input('username');
           if(User::where('username','=',$username)->first()){
               return $this->success('该用户已存在');
           }
           $data['code'] = 2;
           return $data;
        }
        return $this->error('数据丢失,联系管理员');
    }


    public function add(Request $request){
        if($request->isMethod('post')){
            $arr = $request->except(['_token','repass']);
            if($request->boolean('is_admin')){
                $arr['is_admin'] = '1';
            }else{
                $arr['is_admin'] = '0';
            }
            $arr['password'] = Hash::make(md5('lb'.$arr['password']));
            $arr['create_time'] = $arr['update_time'] = date('Y-m-d H:i:s',time());
            $arr['create_id'] = $arr['last_update_id'] = session('user.id');
            if(User::insert($arr)){
                return $this->success('添加成功',asset('/user/index'));
            }else{
                return $this->error('添加失败');
            }
        }
        $roleList = Role::getRoleList();
        return view('admin.user.add',['roleList'=>$roleList]);
    }

    public function edit(Request $request){
        if($request->isMethod('post')){
            $arr = $request->except(['_token','id','password']);
            if(!$request->filled('id')){
                return $this->error('数据丢失');
            }
            $arr['last_update_id'] = session('user.id');
            $arr['update_time'] = date('Y-m-d H:i:s',time());
            if($request->boolean('is_admin')){
                $arr['is_admin'] = '1';
            }else{
                $arr['is_admin'] = '0';
            }
            if($request->filled('password')){
                $arr['password'] = md5(Hash::make('lb'.$request->get('password')));
            }
            if(User::where('id','=',$request->get('id'))->update($arr)){
                return $this->success('编辑成功',asset('/user/index'));
            }
            return $this->error('编辑失败');
        }
        if(!$request->filled('id')){
            return $this->error('数据丢失',asset('/user/index'));
        }
        $roleList = Role::getRoleList();
        $userinfo = User::find($request->input('id'))->toArray();
        unset($userinfo['password']);
        return view('admin.user.edit',['userinfo'=>json_encode($userinfo),'roleList'=>$roleList]);
    }
    public function del(Request $request){
        if($request->filled('id')){
            if(User::where('id','=',$request->id)->update(['status'=>'0'])){
                return $this->success('删除成功',asset('/user/index'));
            }
            return $this->error('删除失败');
        }
        return $this->fail('数据丢失');
    }
}
