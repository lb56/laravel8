<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Cache;
use App\Models\RoleAuth;
use Illuminate\Support\Facades\Hash;

class BaseController extends Controller
{
    //
    public function __construct(Request $request){
        //不需要登录的路由
//        echo Hash::make(md5('lbicas123'));die;
        $except = ['/manage/login'];
        $path = '/'.$request->path();
        if(!in_array($path,$except) && !session('user')){
            return $this->success('请先登录','/manage/login');
        }
//        获取用户权限
        if(!(session('user.role_id') == 1)){//超级管理员
            $authList = RoleAuth::getUserAuthList(session('user.role_id'));
            $authList = array_merge($authList,['/manage','/manage/info','/manage/welcome','/user/check']);
            if(!in_array($path,$authList) && !in_array($path,$except)){
                $str = strpos($path,'/',2);
                $str = substr($path,0,$str);
                $str = $str.'/index';
                //不采用弹窗样式时，可以使用error
//                return $this->error('暂无权限');
                return $this->error1('暂无权限',$str);
            }
        }
//        dump($authList);die;
//        $request->session()->flush();
//        dump($request->session()->all());die;
//        $userinfos['id'] = 1;
//        $userinfos['name'] = 'hh';
//        $userinfos['role_id'] = '1';
//        session(['user'=>$userinfos]);
    }

    protected function dispatch_success_tmpl() {
        return 'admin.error';
    }

    protected function dispatch_error_tmpl() {
        return 'admin.error';
    }
    protected function dispatch_error_tmpl1() {
        return 'admin.error1';
    }

// 操作成功跳转的快捷方法
    protected function success($msg = '', $url = null, $data = '', $wait = 3, array $header = []) {
        session()->flash('success', $msg);

        if (is_null($url)) {
            $url = url()->previous();
        }

        $result = [
            'code' => 0,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];

        if(request()->ajax()) {
            $response =  response()->json($result)->withHeaders($header);
        } else {
            $response =  response()->view($this->dispatch_success_tmpl(), $result)->withHeaders($header);
        }

        throw new HttpResponseException($response);
    }

// 操作失败跳转的快捷方法
    protected function error($msg = '', $url = null, $data = '', $wait = 3, array $header = []) {
        session()->flash('warning', $msg);

        if (is_null($url)) {
            $url = request()->ajax() ? '' : 'javascript:history.back(-1);';
        }

        $result = [
            'code' => 1,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];

        if(request()->ajax()) {
            $response = response()->json($result)->withHeaders($header);
        } else {
            $response = response()->view($this->dispatch_error_tmpl(), $result)->withHeaders($header);
        }

        throw new HttpResponseException($response);
    }
    protected function error1($msg = '', $url = null, $data = '', $wait = 3, array $header = []) {
        session()->flash('warning', $msg);

        if (is_null($url)) {
            $url = request()->ajax() ? '' : 'javascript:history.back(-1);';
        }

        $result = [
            'code' => 1,
            'msg'  => $msg,
            'data' => $data,
            'url'  => $url,
            'wait' => $wait,
        ];

        if(request()->ajax()) {
            $response = response()->json($result)->withHeaders($header);
        } else {
            $response = response()->view($this->dispatch_error_tmpl1(), $result)->withHeaders($header);
        }

        throw new HttpResponseException($response);
    }

    public function showList($list,int $count=0,int $code=0,string $msg=''){
        return ['code'=>$code,'msg'=>$msg,'count'=>$count,'data'=>$list];
    }
}
