<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Models\Auth;
use App\Models\RoleAuth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class IndexController extends BaseController
{
    //后台首页
    public function index(){
        return view('admin.index.index');
    }
    public function welcome(){
        return view('admin.index.welcome');
    }
    //后台首页 配置信息获取
    public function info(){
        if(session('user.role_id') == 1){
            return Auth::setAuthinfo();
        }
        return RoleAuth::setAuthinfo();
    }
    //配置信息
    public function system(){
        return view('admin.index.system');
    }
    //ceshi
    public function ceshi(){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1','ID');
        $sheet->setCellValue('B1','姓名');
        $sheet->setCellValue('C1','年龄');
        $sheet->setCellValue('D1','身高');
        $title = ['ID', 'name', 'age','height'];
        $data = [
                ['ID'=>1,'name'=>'李雷','age'=>'18岁','height'=>'188cm'],
                ['ID'=>2,'name'=>'zhangsna1','age'=>'30','height'=>'221'],
                ['ID'=>3,'name'=>'fdnads','age'=>'12','height'=>'32']
        ];
        foreach ($title as $key => $value) {
            $sheet->setCellValueByColumnAndRow($key+1, 1, $value);
        }

        $row = 2; //从第二行开始
        foreach ($data as $item) {
            $column = 1;
            foreach ($item as $value) {
                $sheet->setCellValueByColumnAndRow($column, $row, $value);
                $column++;
            }
            $row++;
        }


        $writer = new Xlsx($spreadsheet);
        $writer->save('hello world.xlsx');
    }
}
